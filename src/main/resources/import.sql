insert into students_data (name) values ('Ram');
insert into students_data (name) values ('Akash');
insert into deans_data (name) values ('Dean Rajesh');
insert into deans_data (name) values ('Dean Shashi');
insert into slots_data (dean_id, slot, is_available, booked_by_id) values (1, 'Thu, 10 AM', 'true', 0);
insert into slots_data (dean_id, slot, is_available, booked_by_id) values (2, 'Fri, 10 AM', 'true', 0);