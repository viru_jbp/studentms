package com.example.studentms.repo;

import com.example.studentms.domain.Dean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeanRepo extends JpaRepository<Dean, Integer> {
}
