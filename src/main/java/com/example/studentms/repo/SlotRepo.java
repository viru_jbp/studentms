package com.example.studentms.repo;

import com.example.studentms.domain.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SlotRepo extends JpaRepository<Slot, Integer> {

    @Query(nativeQuery = true, value = "SELECT * FROM slots_data where is_available = true")
    Optional<List<Slot>> findAvailableSlot();
    @Query(nativeQuery = true, value = "SELECT * FROM slots_data where is_available = false and dean_id=:deanId")
    Optional<List<Slot>> findDeanViewSlot(@Param("deanId") int deanId);
}
