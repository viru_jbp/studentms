package com.example.studentms.domain;

import javax.persistence.*;

public class BookedSlot {

    private String studentName;
    private String bookedSlot;

    public BookedSlot(String studentName, String bookedSlot) {
        this.studentName = studentName;
        this.bookedSlot = bookedSlot;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getBookedSlot() {
        return bookedSlot;
    }

    public void setBookedSlot(String bookedSlot) {
        this.bookedSlot = bookedSlot;
    }
}
