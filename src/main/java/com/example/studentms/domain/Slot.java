package com.example.studentms.domain;

import javax.persistence.*;

@Entity
@Table(name = "slots_data")
public class Slot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer deanId;
    private String slot;
    private boolean isAvailable;
    private Integer bookedById; // student_id

    public Integer getDeanId() {
        return deanId;
    }

    public void setDeanId(Integer deanId) {
        this.deanId = deanId;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public Integer getBookedById() {
        return bookedById;
    }

    public void setBookedById(Integer bookedById) {
        this.bookedById = bookedById;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
