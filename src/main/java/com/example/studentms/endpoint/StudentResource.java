package com.example.studentms.endpoint;

import com.example.studentms.domain.BookedSlot;
import com.example.studentms.domain.Dean;
import com.example.studentms.domain.Slot;
import com.example.studentms.domain.Student;
import com.example.studentms.repo.DeanRepo;
import com.example.studentms.repo.SlotRepo;
import com.example.studentms.repo.StudentRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class StudentResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(StudentResource.class);

    @Autowired
    StudentRepo studentRepo;

    @Autowired
    DeanRepo deanRepo;

    @Autowired
    SlotRepo slotRepo;

    @GetMapping("/students")
    public List<Student>getAllStudents(){
        LOGGER.info("Fetching all student from DB");
        return studentRepo.findAll();
    }

    @GetMapping("/student/{id}")
    public ResponseEntity<Student>getSingleStudent(@PathVariable Integer id){
        LOGGER.info("Fetch single student from DB based on id");
        Optional<Student> studentFound  = studentRepo.findById(id);
        if(studentFound.isPresent()){
            LOGGER.info("Student found");
            return ResponseEntity.ok(studentFound.get());
        }
        LOGGER.info("Student not found by id ", id);
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/students")
    public ResponseEntity<Student>addStudent(@RequestBody Student student) throws URISyntaxException {
        LOGGER.info("Saving Student");
        student.setId(null);
        Student saveStudent = studentRepo.save(student);
        return ResponseEntity.created(new URI(saveStudent.getId().toString())).body(saveStudent);
    }

    @DeleteMapping("/deleteStudent/{id}")
    public ResponseEntity<String> deleteStudent(@PathVariable Integer id)  {
        LOGGER.info("Delete Student");
        Optional<Student> studentFound  = studentRepo.findById(id);
        if(studentFound.isPresent()){
            studentRepo.deleteById(id);
            return ResponseEntity.ok("Student deleted by " + id);
        }
        LOGGER.info("Student not found by id ", id);
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/updateStudent/{id}")
    public ResponseEntity<Student>updateStudent(@PathVariable Integer id, @RequestBody Student studentDetails){
        LOGGER.info("Fetch single student from DB based on id");
        Optional<Student> studentFound  = studentRepo.findById(id);
        if(studentFound.isPresent()){
            LOGGER.info("Student found");
            Student student = studentFound.get();
            student.setName(studentDetails.getName());
            studentRepo.save(student);
            return ResponseEntity.ok(student);
        }
        LOGGER.info("Student not found by id ", id);
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/hello")
    public String getHello() {
        System.out.println("****************");
        return "Hello World!";
    }

    @GetMapping("/sessions")
    public List<Slot>getSessions() {
        LOGGER.info("Fetching all dean slots from DB");
        Optional<List<Slot>> slots = slotRepo.findAvailableSlot();
        if(slots.isPresent()){
            return slots.get();
        }
        ArrayList<Slot> slotArr = new ArrayList<>();
        Slot slot = new Slot();
        slot.setSlot("Not Available");
        slotArr.add(slot);
        return slotArr;
    }

    @GetMapping("/deans")
    public List<Dean>getAllDeans(){
        LOGGER.info("Fetching all deans from DB");
        return deanRepo.findAll();
    }

    @PostMapping("/bookSlot/{student_id}")
    public ResponseEntity<String> bookSlot(@PathVariable Integer student_id, @RequestBody Slot slot){
        slot.setAvailable(false);
        slot.setBookedById(student_id);
        slotRepo.save(slot);
        return ResponseEntity.ok("Slot booked");
        //return ResponseEntity.notFound().build();
    }

    @GetMapping("/deanViewSlot/{dean_id}")
    public List<BookedSlot> getDeanViewSlot(@PathVariable Integer dean_id) {
        LOGGER.info("Fetching all dean slots from DB");
        Optional<List<Slot>> slotOpt = slotRepo.findDeanViewSlot(dean_id);

        if (slotOpt.isPresent()) {
            return slotOpt.get().stream()
                    .map(slot -> {
                        String slotDetails = slot.getSlot();
                        return studentRepo.findById(slot.getBookedById())
                                .map(student -> {
                                    return new BookedSlot(student.getName(), slotDetails);
                                })
                                .orElse(null);
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }

        // No pending slots
        return Collections.singletonList(new BookedSlot("", "No pending slots"));
    }

/*    @GetMapping("/deanViewSlot/{dean_id}")
    public List<BookedSlot>getDeanViewSlot(@PathVariable Integer dean_id) {
        LOGGER.info("Fetching all dean slots from DB");
        Optional<List<Slot>> slotOpt = slotRepo.findDeanViewSlot(dean_id);
        if(slotOpt.isPresent()){
            List<BookedSlot> bookedSlots = new ArrayList<>();
            List<Slot> slot = slotOpt.get();
            for(int i = 0; i < slot.size(); i++){
                String slotDetails = slot.get(i).getSlot();
                Optional<Student> student = studentRepo.findById(slot.get(i).getBookedById());
                if(student.isPresent()){
                    BookedSlot bookedSlot = new BookedSlot();
                    bookedSlot.setStudentName(student.get().getName());
                    bookedSlot.setBookedSlot(slotDetails);
                    bookedSlots.add(bookedSlot);
                }
            }
            return bookedSlots;
        }
        ArrayList<BookedSlot> slotArr = new ArrayList<>();
        BookedSlot slot = new BookedSlot();
        slot.setBookedSlot("No pending slots");
        slotArr.add(slot);
        return slotArr;
    }*/


}
