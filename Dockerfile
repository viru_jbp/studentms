FROM openjdk:11-jdk-slim
COPY studentms-0.0.1-SNAPSHOT.jar studentms.jar
ENTRYPOINT ["java","-jar","/studentms.jar"]
